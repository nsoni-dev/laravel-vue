<?php
namespace App\Http\Middleware;

use Closure;
use Auth;
use App\Models\User;
use App\Models\Permission;
use App\Models\PermissionRole;

class Acl {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null) {
        // Get the required roles from the route
        $perm = $this->getRequiredRoleForRoute($request->route());
        // Check if a role is required for the route, and
        // if so, ensure that the user has that role.
        $role = "";
        if(@Auth::user()->role_type)
            $role = Auth::user()->role_type;
//dd($perm);
        if (($role == env('SUPERADMIN')) || (!empty($this->checkPerms($perm))) || (empty($perm))) {
            //dd($role, env('SUPERADMIN'));
            return $next($request);
        }
        return redirect()->intended('not-authorized');
    }

    private function getRequiredRoleForRoute($route) {
        $actions = $route->getAction();
        return isset($actions['permissions']) ? $actions['permissions'] : null;
    }

    private function checkPerms($perm) {
        if (is_string($perm)) {
            if(@Auth::user()->role_id)
            $roleId = Auth::user()->role_id;
            if (!empty($roleId)) {
                $permsId = Permission::select('id','label')->where('label', $perm)->first();
                if(!empty($permsId)){
                    $rolePerms = PermissionRole::where(['role_id' => $roleId, 'permission_id' => $permsId->id])->first();
                    if(!empty($rolePerms))
                        return count((array)$rolePerms);
                } else
                    return 0;
            } else {
                return 0;
            }
        }
    }
}