<?php
namespace App\Http\Middleware;

use Closure;
use Auth;
use App\Models\User;
use App\Models\Role;

class SuperAdmin {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null) {
        if (isset(Auth::user()->role_type) && Auth::user()->role_type == env('SUPERADMIN')) {
            return $next($request);
        }
        return redirect()->intended('not-authorized');
    }

}