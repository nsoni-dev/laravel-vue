<?php
namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use Session;
use Image;
use App\Admin;
use App\Models\Posts;
use App\Models\Category;
use App\Models\Tag;
use App\Models\PostCategory;
use App\Models\PostTags;
use App\Events\E_NewPost;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;

class PostController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:sanctum')->except(['index', 'show']); // Add exceptions as needed
    }

	/*Function for listing data*/
	public function index() {
		try {
			$request = request();
			$title = $request->input('title', '');
			$limit = $request->input('limit', 25);
			$offset = $request->input('offset', 0);
            $skip = ($offset-1)*$limit;

			$count = Posts::all()->count();

			if ($title !== '') { // Using strict comparison to check if title is not empty
				$posts = Posts::where('title', 'like', "%$title%")
					->withTrashed()
					->with('get_category') // Assuming 'category' is the relationship name
					->orderByDesc('id') // Using 'orderByDesc' for descending order
					->skip($skip)->take($limit)->get();
			} else {
				$posts = Posts::withTrashed()
					->with('get_category') // Assuming 'category' is the relationship name
					->orderByDesc('id') // Using 'orderByDesc' for descending order
					->skip($skip)->take($limit)->get();
			}

			if ($posts->count() > 0) {
				return response()->json([
					'message' => 'Data fetched successfully.',
					'status' => true,
					'data' => $posts,
                    'count' => $count,
                    'offset' => $offset,
                    'limit' => $limit,
				], 200);
			} else {
				return response()->json([
					'message' => 'No posts available to show.',
					'status' => false,
					'data' => null,
                    'offset' => $offset,
                    'limit' => $limit,
				], 404);
			}
		} catch (Exception $e) {
			return response()->json([
				'message' => 'Something went wrong.',
				'status' => false,
				'data' => null
			], 500);
		}
	}

	public function view($id=null) {
		try {
			$detail = Posts::withTrashed()->with('get_category')->with('get_tag')->find($id);
			if(!empty($detail)){
				$post['id'] = $detail->id;
				$post['title'] = $detail->title;
				$post['post_tagline'] = $detail->post_tagline;
				$post['slug'] = $detail->slug;
				$post['meta_title'] = $detail->meta_title;
				$post['meta_description'] = $detail->meta_description;
				$post['meta_keyword'] = $detail->meta_keyword;
				$post['featured_image'] = $detail->featured_image;
				$post['featured_post'] = $detail->featured_post;
				$post['description'] = $detail->description;
				$post['short_description'] = $detail->short_description;
				$post['tag'] = $detail->tag;
				$post['viewed_count'] = $detail->viewed_count;
				$post['status'] = $detail->status;
				$post['categories'] = array();
				foreach($detail->get_category AS $category){
					$post['categories'][$category->PostCategories->id] = $category->PostCategories->category_name;
					$post['parent_category_id'] = $category->PostCategories->parent_id;
				}
				foreach($detail->get_tag AS $tags){
					$post['tags'][$tags->postTags->id] = $tags->postTags->tag_name;
				}
				return response()->json([
					'message' => 'Data fetched successfully.',
					'status' => true,
					'data' => $post,
				], 200);
			} else {
				return response()->json([
					'message' => 'No posts available to show.',
					'status' => false,
					'data' => null
				], 404);
			}
		} catch (Exception $e) {
			return response()->json([
				'message' => 'Something went wrong.',
				'status' => false,
				'data' => null
			], 500);
		}
	}

	/*Function for store data*/
	public function store(Request $request){
		try{
			$request_data = $request->All();
			$validator = $this->posts_rules($request_data,'add',null);
			if ($validator->fails()) {
				return response()->json([
					'message' => $validator->getMessageBag(),
					'status' => false,
				], 422);
			} else {

				$featured_image = '';
				if($request->hasFile('featured_image')) {
					$photo = $request->file('featured_image');
					$imagename = time(). '-' .str_replace(' ', '-', $photo->getClientOriginalName());
					try
					{
						$folder_path = env('POST_COVER_PATH');
						//$imagename = time().'.'.$photo->getClientOriginalExtension(); 

						$destinationPath = public_path($folder_path.'thumb_750');
						$thumb_img = Image::make($photo->getRealPath())->resize(750, 421);
						$thumb_img->save($destinationPath.'/'.$imagename,80);

						$destinationPath = public_path($folder_path.'thumb_250');
						$thumb_img = Image::make($photo->getRealPath())->resize(320, 179);
						$thumb_img->save($destinationPath.'/'.$imagename,80);

						$destinationPath = public_path($folder_path);
						$photo->move($destinationPath, $imagename);
						$featured_image = $imagename;
					}
					catch(Exception $e)
					{
						return response()->json([
							'message' => 'Error Occured while uploading image.',
							'status' => false,
						], 500);
					}
				}

				$insertData = new Posts;
				$insertData->title = ($request->title) ? $request->title:null;
				$insertData->post_tagline = ($request->post_tagline) ? $request->post_tagline:null;
				$insertData->slug = urlencode($request->slug);
				$insertData->short_description = $request->short_description;
				$insertData->description = $request->description;
				$insertData->featured_image = $featured_image;
				$insertData->meta_title = ($request->meta_title) ? $request->meta_title:null;
				$insertData->meta_keyword = ($request->meta_keyword) ? $request->meta_keyword:null;
				$insertData->meta_description = ($request->meta_description) ? $request->meta_description:null;
				$insertData->tag = ($request->tag) ? $request->tag:null;
				$insertData->added_by = Auth::guard('admin')->user()->id;
				$insertData->status = ($request->status) ? $request->status:'draft';
				$insertData->created_at = date('Y-m-d H:i:s');
				$inserted = $insertData->save();
				if ($inserted) {

					event(new E_NewPost($insertData, $request));

					if (@isset($request->posts) && count($request->posts)) {
						foreach ($request->posts as $category) {
							$PostCategories = PostCategory::create([
								'post_id' => $insertData->id,
								'category_id' => $category
							]);
						}
					}

					if (@isset($request->tag) && count(array($request->tag))) {
						$tags = explode(',', $request->tag);
						foreach ($tags as $tag) {
							$TagObj = new Tag;
							$tag_id = $TagObj->TagExistOrInsert($tag);
							if(!empty($tag_id))
								$PostTags = PostTags::create([
									'post_id' => $insertData->id,
									'tag_id' => $tag_id
								]);
						}
					}


					return response()->json([
						'message' => 'Record has been successfully updated.',
						'status' => true,
					], 200);
				} else {
					return response()->json([
						'message' => 'Please try again.',
						'status' => false,
					], 404);
				}
				
			}
		} catch (Exception $e) {
			return response()->json([
				'message' => 'Something went wrong.',
				'status' => false,
				'data' => null
			], 500);
		}
	}

	/*Function for update store data*/
	public function update(Request $request,$id){
		try {
			$request_data = $request->All();
			$validator = $this->posts_rules($request_data,'update',null);
			if ($validator->fails()) {
				return response()->json([
					'message' => $validator->getMessageBag(),
					'status' => false,
				], 422);

			} else {
				$updateData = Posts::find($id);

				$featured_image = '';
				if($request->hasFile('featured_image')) {
					$photo = $request->file('featured_image');
					$folder_path = env('POST_COVER_PATH');
					try
					{
						$imagename = time(). '-' .str_replace(' ', '-', $photo->getClientOriginalName());
						//$imagename = time().'.'.$photo->getClientOriginalExtension(); 

						$destinationPath = public_path($folder_path.'/thumb_750');
						$thumb_img = Image::make($photo->getRealPath())->resize(750, 421);
						$thumb_img->save($destinationPath.'/'.$imagename,80);

						$destinationPath = public_path($folder_path.'/thumb_250');
						$thumb_img = Image::make($photo->getRealPath())->resize(320, 179);
						$thumb_img->save($destinationPath.'/'.$imagename,80);

						$destinationPath = public_path($folder_path);
						$photo->move($destinationPath, $imagename);
						$featured_image = $imagename;
					}
					catch(Exception $e)
					{
						return response()->json([
							'message' => 'Error Occured while uploading image.',
							'status' => false,
						], 500);
					}
					if ((file_exists('/public'.$folder_path.'/thumb_750/'.$updateData->featured_image)) && is_file('/public'.$folder_path.'/thumb_750/'.$updateData->featured_image))
					{
						unlink('/public'.$folder_path.'/thumb_750/'.$updateData->featured_image);
					}
					if ((file_exists('/public'.$folder_path.'/thumb_250/'.$updateData->featured_image)) && is_file('/public'.$folder_path.'/thumb_250/'.$updateData->featured_image))
					{
						unlink('/public'.$folder_path.'/thumb_250/'.$updateData->featured_image);
					}
				} else {
					$featured_image = $updateData->featured_image;
				}

				$updateData->title = ($request->title) ? $request->title:null;
				$updateData->post_tagline = ($request->post_tagline) ? $request->post_tagline:null;
				$updateData->slug = urlencode($request->slug);
				$updateData->short_description = $request->short_description;
				$updateData->description = $request->description;
				$updateData->featured_image = $featured_image;
				$updateData->meta_title = ($request->meta_title) ? $request->meta_title:null;
				$updateData->meta_keyword = ($request->meta_keyword) ? $request->meta_keyword:null;
				$updateData->meta_description = ($request->meta_description) ? $request->meta_description:null;
				$updateData->tag = ($request->tag) ? $request->tag:null;
				$updateData->status = ($request->status) ? $request->status:'draft';
				$updateData->updated_at = date('Y-m-d H:i:s');
				$updated = $updateData->save();
				if ($updated) {
					if (@isset($request->posts) && count($request->posts)) {
						$deleteOldCat = PostCategory::where("post_id", $id)->delete();
						foreach ($request->posts as $posts) {
							$PostCategory = PostCategory::create([
								'post_id' => $id,
								'category_id' => $posts
							]);
						}
					}

					if (@isset($request->tag) && count(array($request->tag))) {

						$deleteOldTags = PostTags::where("post_id", $id)->delete();
						$tags = explode(',', $request->tag);
						foreach ($tags as $tag) {
							$TagObj = new Tag;
							$tag_id = $TagObj->TagExistOrInsert($tag);
							if(!empty($tag_id))
								$PostTags = PostTags::create([
									'post_id' => $id,
									'tag_id' => $tag_id
								]);
						}
					}
					return response()->json([
						'message' => 'Record has been successfully updated.',
						'status' => true,
					], 200);
				} else {
					return response()->json([
						'message' => 'Please try again.',
						'status' => false,
					], 404);
				}
				
			}
		} catch (Exception $e) {
			return response()->json([
				'message' => 'Something went wrong.',
				'status' => false,
				'data' => null
			], 500);
		}
	}

	public function upload(Request $request)
	{
			$imgpath = null;
			if($request->hasFile('file')){
				$photo = $request->file('file');
				$folder_path = env('POST_COVER_PATH');
				$destinationPath = public_path($folder_path.'/inner');
				$imagename = time(). '-' .$photo->getClientOriginalName();
				$photo->move($destinationPath, $imagename);
				$imgpath = env('APP_URL').env('POST_COVER_PATH').'inner/'.$imagename;
				$image = json_encode(['location' => $imgpath]);
			}

			if ($imgpath) {
				return response()->json([
					'message' => 'Image uploaded successfully.',
					'status' => true,
					'location' => $imgpath,
				], 200);
			} else {
				return response()->json([
					'message' => 'Image uploaded failed.',
					'status' => false,
					'location' => null
				], 404);
			}

	}

    public function getSubCategory($categoryId){
        $template = $this->_getSubCategoryTemplate($categoryId);
        return $template;
    }
    
    private function _getSubCategoryTemplate($categoryId){
        $subposts = Category::where('parent_id', $categoryId)->get()->toArray();
        return view('admin.posts.subcategory', compact('subposts'));
    }

	/**
	* Archive specific category
	*
	* @param $id
	*
	* @return \Illuminate\Http\RedirectResponse
	*/
	public function archive($id) {
		$Post = Posts::find(decryptId($id));
		if ($category) {
			$archived = $Post->delete();
			if ($archived) {
				Session::flash('success', 'Post has been successfully archived.');
				return redirect()->route('admin.category.manage');
			} else {
				Session::flash('error', 'Please try again.');
				return redirect()->route('admin.post.manage');
			}
		}
		Session::flash('error', 'Post not found.');
		return redirect()->route('admin.post.manage');
	}

	/**
	* Restore specific category
	*
	* @param $id
	*
	* @return \Illuminate\Http\RedirectResponse
	*/
	public function restore($id) {
		$Post = Posts::onlyTrashed()->find(decryptId($id));
		if ($Post) {
			$restored = $Post->restore();
			if ($restored) {
				Session::flash('success', 'Post has been successfully restored.');
				return redirect()->route('admin.post.manage');
			} else {
				Session::flash('error', 'Please try again.');
				return redirect()->route('admin.post.manage');
			}
		}
		Session::flash('error', 'Post not found.');
		return redirect()->route('admin.post.manage');
	}

	public function posts_rules(array $data,$method,$id=null){
		$messages = [
			'title.required' => 'Post title is required.',
			'slug.required' => 'Post URL is required.',
			'description.required' => 'Description is required.',
			'featured_image.required' => 'Featured image is required.',
			'status.required' => 'The status is required.'
		];
		switch($method){
			case 'add':
				$validator = Validator::make($data, [
					'title' => 'required|unique:posts|max:255',
					'slug' => 'required|unique:posts|max:255',
					'description' => 'required',
					'featured_image' => 'required',
					'status' => 'required',
				], $messages);
				break;
			case 'update':
				$validator = Validator::make($data, [
					'title' => 'required',
					'slug' => 'required',
					'description' => 'required',
					'status' => 'required',
				], $messages);
				break;
			default:
		}
		return $validator;
	}

}
