<?php
namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Auth;
use Session;
use Str;
use Illuminate\Http\Request;
use App\Models\ContactUs;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;

class ContactUsController extends Controller
{
  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
    $this->middleware('auth:sanctum')->except(['index', 'show']); // Add exceptions as needed
  }

  /**
   * Show the application dashboard.
   *
   * @return \Illuminate\Http\Response
   */
  public function index() {
    $request = request();
    $username = $request->input('username', '');
    $useremail = $request->input('useremail', '');
    $status = $request->input('status', '');
    $page = $request->input('page', 1);
    $limit = $request->input('limit', 25);
    $skip = ($page - 1) * $limit;

    $contactusSql = ContactUs::query();

    if ($username !== '') {
        $contactusSql->where('username', 'like', "%$username%");
    }
    if ($useremail !== '') {
        $contactusSql->where('useremail', 'like', "%$useremail%");
    }
    if ($status !== '') {
      if ($status == 'Active') {
        // Query for active categories (not including soft deleted)
        $contactusSql->whereNull('deleted_at');
      } elseif ($status == 'Trashed') {
        // Query for soft deleted categories
        $contactusSql->onlyTrashed();
      }
    } else {
      $contactusSql->withTrashed();
    }

    $count = $contactusSql->count(); // Count filtered contactus

    $contactus = $contactusSql
        ->orderByDesc('id')
        ->skip($skip)
        ->take($limit)
        ->get();

    try {
      if ($contactus->count() > 0) {
        return response()->json([
          'message' => 'Data fetched successfully.',
          'status' => true,
          'data' => $contactus,
          'count' => $count,
          'page' => $page,
          'limit' => $limit,
        ], 200);
      } else {
        return response()->json([
          'message' => 'No contactus available to show.',
          'status' => false,
          'data' => null,
          'count' => $count,
          'page' => $page,
          'limit' => $limit,
        ], 404);
      }
    } catch (Exception $e) {
      return response()->json([
        'message' => 'Something went wrong.',
        'status' => false,
        'data' => null,
      ], 500);
    }
  }
}