<?php
namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Carbon;
use Notifiable;
use Auth;
use App\Models\User;

class AuthController extends Controller
{
    /**
     * Create user
     *
     * @param  [string] name
     * @param  [string] email
     * @param  [string] password
     * @param  [string] password_confirmation
     * @return [string] message
     */
    public function register(Request $request)
    {
        try{
            $validation = Validator::make($request->all(),[
                'name' => ['required', 'string', 'max:255'],
                'email' => ['required', 'string', 'email', 'max:255'],
                'password' => ['required', 'string', 'min:8', 'confirmed'],
                'password_confirmation' => ['required', 'string'],
            ]);
            if($validation->fails()){
                return response()->json([
                    'message'=>'Oops! Please try again',
                    'status'=>false,
                    "data"=>array()
                ], 207);
            } else {
                $user = User::where('email',$request->email)->first();
                if(!empty($user)){

                    $userData = array();
                    $userData['user_id'] = $user->id;
                    $userData['name'] = $user->name;
                    $userData['email'] = $user->email;

                    return response()->json([
                        'message'=>'You are already registered with us.',
                        'code'=>207,
                        'status' => false,
                        'user_id' => $user->id,
                        'data' => $userData,
                    ], 207);
                } else {

                    $user = new User([
                        'name' => $request->name,
                        'email' => $request->email,
                        'password' => bcrypt($request->password)
                    ]);

                    $user->save();

                    $userData = array();
                    $userData['user_id'] = $user->id;
                    $userData['name'] = $user->name;
                    $userData['email'] = $user->email;

                    //event(new E_NewUser($user, $request));

                    return response()->json([
                        'message'=>'You have registered successfully.',
                        'status' => true,
                        'user_id' => $user->id,
                        'data' => $userData,
                    ], 201);
                }
            }

        }
        catch(Exception $e){
            return response()->json([
                'message'=>'Something went wrong.',
                'status'=>false,
                "data"=>null,
                'code'=>500,
            ], 500);
        }
    }
 
    /**
     * Login user and create token
     *
     * @param Request $request
     *
     * @param  [string] email
     * @param  [string] password
     * @param  [boolean] remember_me
     * @return [string] access_token
     * @return [string] token_type
     * @return [string] expires_at
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required|string|email',
            'password' => 'required|string',
            'remember_me' => 'boolean'
        ]);

        $credentials = request(['email', 'password']);

        if (!Auth::attempt($credentials))
            return response()->json([
                'message' => 'Unauthorized Access, please confirm credentials or verify your email'
            ], 401);

        $user = $request->user();

        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->accessToken;
    
        if ($request->remember_me) {
            $token->expires_at = Carbon::now()->addWeeks(1);
            $token->save();
        }

        $token->save();
        return response()->json([
            'success' => true,
            'id' => $user->id,
            'name' => $user->name,
            'email' => $user->email,
            'access_token' => $tokenResult->plainTextToken,
            'token_type' => 'Bearer',
            'expires_at' => Carbon::parse($tokenResult->accessToken->expires_at)->toDateTimeString()
        ], 201);
    }
 
    /**
     * Returns Authenticated User profile
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function profile()
    {
        return response()->json(['user' => auth()->user()], 200);
    }

	public function change_password(Request $request)
	{
	    $input = $request->all();
	    //$userid = Auth::guard('api')->user()->id;
        $userid = auth('sanctum')->user()->id ;
	    $rules = array(
	        'old_password' => 'required',
	        'new_password' => 'required|min:6',
	        'confirm_password' => 'required|same:new_password',
	    );
	    $validator = Validator::make($input, $rules);
	    if ($validator->fails()) {
	        $arr = array("status" => 400, "message" => $validator->errors()->first(), "data" => array());
	    } else {
	        try {
	            if ((Hash::check(request('old_password'), Auth::user()->password)) == false) {
	                $arr = array("status" => 400, "message" => "Check your old password.", "data" => array());
	            } else if ((Hash::check(request('new_password'), Auth::user()->password)) == true) {
	                $arr = array("status" => 400, "message" => "Please enter a password which is not similar then current password.", "data" => array());
	            } else {
	                User::where('id', $userid)->update(['password' => Hash::make($input['new_password'])]);
	                $arr = array("status" => 200, "message" => "Password updated successfully.", "data" => array());
	            }
	        } catch (\Exception $ex) {
	            if (isset($ex->errorInfo[2])) {
	                $msg = $ex->errorInfo[2];
	            } else {
	                $msg = $ex->getMessage();
	            }
	            $arr = array("status" => 400, "message" => $msg, "data" => array());
	        }
	    }
	    return \Response::json($arr);
	}

	public function forgot_password(Request $request)
	{
	    $input = $request->all();
	    $rules = array(
	        'email' => "required|email",
	    );
	    $validator = Validator::make($input, $rules);
	    if ($validator->fails()) {
	        $arr = array("status" => 400, "message" => $validator->errors()->first(), "data" => array());
	    } else {
	        try {
	            $response = Password::sendResetLink($request->only('email'), function (Message $message) {
	                $message->subject($this->getEmailSubject());
	            });
	            switch ($response) {
	                case Password::RESET_LINK_SENT:
	                    return \Response::json(array("status" => 200, "message" => trans($response), "data" => array()));
	                case Password::INVALID_USER:
	                    return \Response::json(array("status" => 400, "message" => trans($response), "data" => array()));
	            }
	        } catch (\Swift_TransportException $ex) {
	            $arr = array("status" => 400, "message" => $ex->getMessage(), "data" => []);
	        } catch (Exception $ex) {
	            $arr = array("status" => 400, "message" => $ex->getMessage(), "data" => []);
	        }
	    }
	    return \Response::json($arr);
	}

    protected function getUserProfile($user_id){
        $user = User::find($user_id);

        $userData = array();
        $userData['user_id'] = $user->id;
        $userData['name'] = $user->name;
        $userData['email'] = $user->email;

        return $userData;
    }

}
