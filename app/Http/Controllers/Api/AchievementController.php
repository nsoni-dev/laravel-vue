<?php
namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Auth;
use Session;
use Str;
use Illuminate\Http\Request;
use App\Models\Achievement;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;

class AchievementController extends Controller
{
  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
    $this->middleware('auth:sanctum')->except(['index', 'show']); // Add exceptions as needed
  }

  /**
   * Show the application dashboard.
   *
   * @return \Illuminate\Http\Response
   */
  public function index() {
    $request = request();
    $course_type = $request->input('course_type', '');
    $useremail = $request->input('useremail', '');
    $status = $request->input('status', '');
    $page = $request->input('page', 1);
    $limit = $request->input('limit', 25);
    $skip = ($page - 1) * $limit;

    $AchievementSql = Achievement::query();

    if ($course_type !== '') {
        $AchievementSql->where('course_type', 'like', "%$course_type%");
    }
    if ($status !== '') {
      if ($status == 'Active') {
        // Query for active categories (not including soft deleted)
        $AchievementSql->whereNull('deleted_at');
      } elseif ($status == 'Trashed') {
        // Query for soft deleted categories
        $AchievementSql->onlyTrashed();
      }
    } else {
      $AchievementSql->withTrashed();
    }

    $count = $AchievementSql->count(); // Count filtered Achievement

    $Achievement = $AchievementSql
        ->orderByDesc('id')
        ->skip($skip)
        ->take($limit)
        ->get();

    try {
      if ($Achievement->count() > 0) {
        return response()->json([
          'message' => 'Data fetched successfully.',
          'status' => true,
          'data' => $Achievement,
          'count' => $count,
          'page' => $page,
          'limit' => $limit,
        ], 200);
      } else {
        return response()->json([
          'message' => 'No Achievement available to show.',
          'status' => false,
          'data' => null,
          'count' => $count,
          'page' => $page,
          'limit' => $limit,
        ], 404);
      }
    } catch (Exception $e) {
      return response()->json([
        'message' => 'Something went wrong.',
        'status' => false,
        'data' => null,
      ], 500);
    }
  }

  public function store(Request $request){
    try {
      $Achievement = Achievement::where('course_type', 'like', "%$request->course_type%")->withTrashed()->first();
      if ($Achievement) {
        return response()->json([
          'message' => 'Course type already exists.',
          'status' => false,
          'data' => null
        ], 500);
      } else {
        $insertData = new Achievement;
				$insertData->course_type = ($request->course_type) ? $request->course_type:null;
				$insertData->parent_id = ($request->parent_id) ? $request->parent_id:null;
				$inserted = $insertData->save();
        if($inserted){
          return response()->json([
            'message' => 'Course type added successfully.',
            'status' => false,
            'data' => $insertData,
          ], 200);
        } else {
          return response()->json([
            'message' => 'Opps! Some error occured.',
            'status' => false,
            'data' => null
          ], 500);
        }
      }
    } catch (Exception $e) {
      return response()->json([
        'message' => 'Something went wrong.',
        'status' => false,
        'data' => null,
      ], 500);
    }
  }
}