<?php
namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Auth;
use Session;
use Str;
use Illuminate\Http\Request;
use App\Models\Tag;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;

class TagController extends Controller
{
  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
    $this->middleware('auth:sanctum')->except(['index', 'show']); // Add exceptions as needed
  }

  /**
   * Show the application dashboard.
   *
   * @return \Illuminate\Http\Response
   */
  public function index() {
    $request = request();
    $tag_name = $request->input('tag_name', '');
    $status = $request->input('status', '');
    $limit = $request->input('limit', 25);
    $offset = $request->input('offset', 0);
    $skip = ($offset - 1) * $limit;

    $tagSql = Tag::query();

    if ($tag_name !== '') {
        $tagSql->where('tag_name', 'like', "%$tag_name%");
    }
    
    if ($status !== '') {
      if ($status == 'Active') {
        // Query for active categories (not including soft deleted)
        $tagSql->whereNull('deleted_at');
      } elseif ($status == 'Trashed') {
        // Query for soft deleted categories
        $tagSql->onlyTrashed();
      }
    } else {
      $tagSql->withTrashed();
    }

    $count = $tagSql->count(); // Count filtered tags

    $tags = $tagSql
        ->orderByDesc('id')
        ->skip($skip)
        ->take($limit)
        ->get();

    try {
      if ($tags->count() > 0) {
        return response()->json([
          'message' => 'Data fetched successfully.',
          'status' => true,
          'data' => $tags,
          'count' => $count,
          'offset' => $offset,
          'limit' => $limit,
        ], 200);
      } else {
        return response()->json([
          'message' => 'No tags available to show.',
          'status' => false,
          'data' => null,
          'count' => $count,
          'offset' => $offset,
          'limit' => $limit,
        ], 404);
      }
    } catch (Exception $e) {
      return response()->json([
        'message' => 'Something went wrong.',
        'status' => false,
        'data' => null,
      ], 500);
    }
  }
}