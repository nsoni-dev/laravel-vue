<?php
namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Auth;
use Session;
use Str;
use Illuminate\Http\Request;
use App\Models\CourseType;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;

class CourseTypeController extends Controller
{
  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
    $this->middleware('auth:sanctum')->except(['index', 'show']); // Add exceptions as needed
  }

  /**
   * Show the application dashboard.
   *
   * @return \Illuminate\Http\Response
   */
  public function index() {
    $request = request();
    $course_type = $request->input('course_type', '');
    $useremail = $request->input('useremail', '');
    $status = $request->input('status', '');
    $page = $request->input('page', 1);
    $limit = $request->input('limit', 25);
    $skip = ($page - 1) * $limit;

    $CourseTypeSql = CourseType::query();

    if ($course_type !== '') {
        $CourseTypeSql->where('course_type', 'like', "%$course_type%");
    }
    if ($status !== '') {
      if ($status == 'Active') {
        // Query for active categories (not including soft deleted)
        $CourseTypeSql->whereNull('deleted_at');
      } elseif ($status == 'Trashed') {
        // Query for soft deleted categories
        $CourseTypeSql->onlyTrashed();
      }
    } else {
      $CourseTypeSql->withTrashed();
    }

    $count = $CourseTypeSql->count(); // Count filtered CourseType

    $CourseType = $CourseTypeSql
        ->orderByDesc('id')
        ->skip($skip)
        ->take($limit)
        ->get();

    try {
      if ($CourseType->count() > 0) {
        return response()->json([
          'message' => 'Data fetched successfully.',
          'status' => true,
          'data' => $CourseType,
          'count' => $count,
          'page' => $page,
          'limit' => $limit,
        ], 200);
      } else {
        return response()->json([
          'message' => 'No CourseType available to show.',
          'status' => false,
          'data' => null,
          'count' => $count,
          'page' => $page,
          'limit' => $limit,
        ], 404);
      }
    } catch (Exception $e) {
      return response()->json([
        'message' => 'Something went wrong.',
        'status' => false,
        'data' => null,
      ], 500);
    }
  }

  public function store(Request $request){
    try {
      $CourseType = CourseType::where('course_type', 'like', "%$request->course_type%")->withTrashed()->first();
      if ($CourseType) {
        return response()->json([
          'message' => 'Course type already exists.',
          'status' => false,
          'data' => null
        ], 500);
      } else {
        $insertData = new CourseType;
				$insertData->course_type = ($request->course_type) ? $request->course_type:null;
				$insertData->parent_id = ($request->parent_id) ? $request->parent_id:null;
				$inserted = $insertData->save();
        if($inserted){
          return response()->json([
            'message' => 'Course type added successfully.',
            'status' => false,
            'data' => $insertData,
          ], 200);
        } else {
          return response()->json([
            'message' => 'Opps! Some error occured.',
            'status' => false,
            'data' => null
          ], 500);
        }
      }
    } catch (Exception $e) {
      return response()->json([
        'message' => 'Something went wrong.',
        'status' => false,
        'data' => null,
      ], 500);
    }
  }

  public function view($id){
		try {
			$detail = CourseType::withTrashed()->find($id);
			if(!empty($detail)){
				$coursetype['id'] = $detail->id;
				$coursetype['course_type'] = $detail->course_type;

        return response()->json([
          'message' => 'Data fetched successfully.',
          'status' => true,
          'data' => $coursetype,
        ], 200);
      } else {
        return response()->json([
          'message' => 'No course type available to show.',
          'status' => false,
          'data' => null
        ], 404);
      }
    } catch (Exception $e) {
      return response()->json([
        'message' => 'Something went wrong.',
        'status' => false,
        'data' => null
      ], 500);
    }
  }

	public function update(Request $request,$id){
		try {
			$request_data = $request->All();
			$validator = $this->posts_rules($request_data,'update',null);
			if ($validator->fails()) {
				return response()->json([
					'message' => $validator->getMessageBag(),
					'status' => false,
				], 422);

			} else {
				$updateData = CourseType::find($id);
				$updateData->course_type = ($request->course_type) ? $request->course_type:null;
				$updateData->updated_at = date('Y-m-d H:i:s');
				$updated = $updateData->save();
				if ($updated) {
					return response()->json([
						'message' => 'Record has been successfully updated.',
						'status' => true,
					], 200);
				} else {
					return response()->json([
						'message' => 'Please try again.',
						'status' => false,
					], 404);
				}
				
			}
		} catch (Exception $e) {
			return response()->json([
				'message' => 'Something went wrong.',
				'status' => false,
				'data' => null
			], 500);
		}
	}

	public function posts_rules(array $data,$method,$id=null){
		$messages = [
			'course_type.required' => 'Post title is required.',
		];
		switch($method){
			case 'add':
				$validator = Validator::make($data, [
					'course_type' => 'required|unique:posts,course_type|max:255',
				], $messages);
				break;
			case 'update':
				$validator = Validator::make($data, [
					'course_type' => 'required|unique:posts|max:255',
				], $messages);
				break;
			default:
		}
		return $validator;
	}
}