<?php
namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Auth;
use Session;
use Str;
use Illuminate\Http\Request;
use App\Models\Department;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;

class DepartmentController extends Controller
{
  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
    $this->middleware('auth:sanctum')->except(['index', 'show']); // Add exceptions as needed
  }

  /**
   * Show the application dashboard.
   *
   * @return \Illuminate\Http\Response
   */
  public function index() {
    $request = request();
    $department_name = $request->input('department_name', '');
    $status = $request->input('status', '');
    $page = $request->input('page', 1);
    $limit = $request->input('limit', 25);
    $skip = ($page - 1) * $limit;

    $DepartmentSql = Department::query();

    if ($department_name !== '') {
        $DepartmentSql->where('department_name', 'like', "%$department_name%");
    }
    if ($status !== '') {
      if ($status == 'Active') {
        // Query for active categories (not including soft deleted)
        $DepartmentSql->whereNull('deleted_at');
      } elseif ($status == 'Trashed') {
        // Query for soft deleted categories
        $DepartmentSql->onlyTrashed();
      }
    } else {
      $DepartmentSql->withTrashed();
    }

    $count = $DepartmentSql->count(); // Count filtered Department

    $Department = $DepartmentSql
        ->orderByDesc('id')
        ->skip($skip)
        ->take($limit)
        ->get();

    try {
      if ($Department->count() > 0) {
        return response()->json([
          'message' => 'Data fetched successfully.',
          'status' => true,
          'data' => $Department,
          'count' => $count,
          'page' => $page,
          'limit' => $limit,
        ], 200);
      } else {
        return response()->json([
          'message' => 'No Department available to show.',
          'status' => false,
          'data' => null,
          'count' => $count,
          'page' => $page,
          'limit' => $limit,
        ], 404);
      }
    } catch (Exception $e) {
      return response()->json([
        'message' => 'Something went wrong.',
        'status' => false,
        'data' => null,
      ], 500);
    }
  }

  public function store(Request $request){
    try {
      $Department = Department::where('department_name', 'like', "%$request->department_name%")->withTrashed()->first();
      if ($Department) {
        return response()->json([
          'message' => 'Course type already exists.',
          'status' => false,
          'data' => null
        ], 500);
      } else {
        $insertData = new Department;
				$insertData->department_name = ($request->department_name) ? $request->department_name:null;
				$insertData->url = ($request->url) ? $request->url:null;
				$inserted = $insertData->save();
        if($inserted){
          return response()->json([
            'message' => 'Course type added successfully.',
            'status' => false,
            'data' => $insertData,
          ], 200);
        } else {
          return response()->json([
            'message' => 'Opps! Some error occured.',
            'status' => false,
            'data' => null
          ], 500);
        }
      }
    } catch (Exception $e) {
      return response()->json([
        'message' => 'Something went wrong.',
        'status' => false,
        'data' => null,
      ], 500);
    }
  }

  public function view($id){
		try {
			$detail = Department::withTrashed()->find($id);
			if(!empty($detail)){
				$department['id'] = $detail->id;
				$department['department_name'] = $detail->department_name;
				$department['url'] = $detail->url;

        return response()->json([
          'message' => 'Data fetched successfully.',
          'status' => true,
          'data' => $department,
        ], 200);
      } else {
        return response()->json([
          'message' => 'No course type available to show.',
          'status' => false,
          'data' => null
        ], 404);
      }
    } catch (Exception $e) {
      return response()->json([
        'message' => 'Something went wrong.',
        'status' => false,
        'data' => null
      ], 500);
    }
  }

}