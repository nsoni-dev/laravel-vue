<?php
namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Auth;
use Session;
use Image;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Models\Category;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;

class CategoryController extends Controller
{
  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
      $this->middleware('auth:sanctum')->except(['index', 'show']); // Add exceptions as needed
  }

  /**
   * Show the application dashboard.
   *
   * @return \Illuminate\Http\Response
   */

  /*Function for list data*/
  public function index() {
    $request = request();
    $category_name = $request->input('category_name', '');
    $type = $request->input('type', '');
    $parent_id = $request->input('parent_id', '');
    $status = $request->input('status', '');
    $limit = $request->input('limit', 25);
    $offset = $request->input('offset', 0);
    $skip = ($offset - 1) * $limit;

    $categorySql = Category::query();

    if ($category_name !== '') {
        $categorySql->where('category_name', 'like', "%$category_name%");
    }
    if ($type == 'Parent') {
      $categorySql->whereNull('parent_id');
    } else if ($type == 'Child') {
      $categorySql->where('parent_id', $parent_id);
    }
    
    if ($status !== '') {
      if ($status == 'Active') {
        // Query for active categories (not including soft deleted)
        $categorySql->whereNull('deleted_at');
      } elseif ($status == 'Trashed') {
        // Query for soft deleted categories
        $categorySql->onlyTrashed();
      }
    } else {
      $categorySql->withTrashed();
    }

    $count = $categorySql->count(); // Count filtered categories

    $categories = $categorySql->with('parent')
        ->orderByDesc('id')
        ->skip($skip)
        ->take($limit)
        ->get();

    try {
      if ($categories->count() > 0) {
        return response()->json([
          'message' => 'Data fetched successfully.',
          'status' => true,
          'data' => $categories,
          'count' => $count,
          'offset' => $offset,
          'limit' => $limit,
        ], 200);
      } else {
        return response()->json([
          'message' => 'No categories available to show.',
          'status' => false,
          'data' => null,
          'count' => $count,
          'offset' => $offset,
          'limit' => $limit,
        ], 404);
      }
    } catch (Exception $e) {
      return response()->json([
        'message' => 'Something went wrong.',
        'status' => false,
        'data' => null,
      ], 500);
    }
  }

}