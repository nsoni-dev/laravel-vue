<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class PostCategory extends Model
{
    public $table = 'post_category';
	protected $guarded = [];
	public $timestamps = false;

    protected $fillable = [
        'post_id', 'category_id'
    ];

    public function postCategories() {
        return $this->belongsTo('App\Models\Category', 'category_id', 'id');
    }

    public function post() {
        return $this->belongsTo('App\Models\Posts', 'post_id', 'id');
    }

    public function categoryPostCount($limit, $offset){
        return $this->select('category_id', DB::raw('count(*) as post_count'))->groupby('category_id')->orderBy('post_count', 'DESC')->limit($limit)->offset($offset)->get();
    }

}