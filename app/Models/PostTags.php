<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PostTags extends Model
{
    public $table = 'post_tags';
	protected $guarded = [];
	public $timestamps = false;

    protected $fillable = [
        'post_id', 'tag_id'
    ];

    public function postTags() {
        return $this->belongsTo('App\Models\Tag', 'tag_id', 'id');
    }

    public function post() {
        return $this->belongsTo('App\Models\Posts', 'post_id', 'id');
    }
}