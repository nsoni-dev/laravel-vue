<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use SoftDeletes;

    public $table = 'categories';
	protected $guarded = [];

    public function PostCategories() {
        return $this->hasMany('App\Models\Posts');
    }

    public function postsCountRelation(){
        return $this->hasOne('App\Models\PostCategory')
                    ->selectRaw('category_id,count(*) as count')
                    ->groupBy("category_id");
    }

    public function children()
    {
        return $this->hasMany('App\Models\Category', 'parent_id', 'id');
    }

    public function parent()
    {
        return $this->belongsTo('App\Models\Category', 'parent_id');
    }    

    /**
     * fetch all blog categories and show it in article-details-sidebar
     */
    public function categories(){

        $allCategories = Category::where([['status', 'active']])->get();

        $categoryList = [];
        $i = 0;
        foreach ($allCategories as $c) {
            $cDetails = new \stdClass();
            $cDetails->category_name = $c->category_name;
            $cDetails->category_slug = $c->category_slug;
            $cDetails->category_icon = $c->category_icon;
            $cDetails->category_color_code = $c->category_color_code;
            $cDetails->count = $c->postsCountRelation()->count();
            $categoryList[$c->id] = $cDetails;
            $i++;
        }

        return $categoryList;
    }
}