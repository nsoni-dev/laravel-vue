<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\SoftDeletes;

class Tag extends Model
{
  use SoftDeletes;

  protected $collection =  'tags';
  protected $fillable =  ['tag_name', 'icon', 'slug','desc'];
  
  public $type = 'tags';

  public function PostTagies() {
    return $this->hasMany('App\Models\Posts');
  }

  public function PostTags() {
    return $this->hasMany('App\Models\PostTags');
  }

  /**
   * fetch all blog categories and show it in article-details-sidebar
   */
  public function getAllTags(){

    $allTags = Tag::orderby("tag_name","asc")->get();

    $tagList = [];
    foreach ($allTags as $t) {
      $tDetails = new \stdClass();
      $tDetails->tag = $t->tag_name;
      $tDetails->slug = $t->tag_slug;
      $tagList[$t->id] = $tDetails;
    }
    return $tagList;
  }

  public function TagExistOrInsert($tag_name){
    $checkExists = Tag::where("tag_name", $tag_name)->get()->first();
    if(!empty($checkExists)){
      return $checkExists->id;
    } else {
      $insertData = new Tag;
      $insertData->tag_name = $tag_name;
      $insertData->tag_slug = Str::slug($tag_name);
      $insertData->meta_title = $tag_name;
      $insertData->meta_keyword = $tag_name;
      $insertData->status = 'active';
      $insertData->created_at = date('Y-m-d H:i:s');
      $insertData->updated_at = date('Y-m-d H:i:s');
      $insertedData = $insertData->save();
      if ($insertedData) {
        return $insertData->id;
      } else {
        return '';
      }
    }
  }
}
