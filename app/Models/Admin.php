<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Auth;

class Admin extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    protected $guard = 'admin';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'phone',
        'password',
        'show_password',
        
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public function get_author(){
        return $this->hasMany('\App\Models\User', 'id', 'added_by');
    }

    public function role()
    {
        return $this->belongsTo(Role::class);
    }

    public function user_role() {
        return $this->belongsTo('\App\Models\Role','role_id','id');
    }

    public function roles() {
        return $this->belongsToMany('\App\Models\Role','role_id','id');
    }

    public function hasRole($role) {
        $role_id = Auth::guard('admin')->user()->role_id;

        $roleType = "";
        $roles = Role::where('id',$role_id)->first();
        
        if ($roles->title == env('SUPERADMIN')) {
            return 1;
        }
        if (!empty($role_id)) {

            if (count($role->toArray())) {
                foreach ($role->toArray() as $val) {
                    if ($val['role_id'] == $role_id) {
                        return 1;
                        break;
                    }
                }
            } else {
                return 0;
            }
        } else {
            return 0;
        }
    }

}
