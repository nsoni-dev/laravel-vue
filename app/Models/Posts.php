<?php
namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use App\Models\Category;
use App\Models\Tag;
use App\Models\PostCategory;
use App\Models\PostTags;
use DB;
use Illuminate\Database\Eloquent\SoftDeletes;

class Posts extends Model
{
    use SoftDeletes;

    public $table = 'posts';
	protected $guarded = [];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title','slug', 'featured_image', 'description', '  status'
    ];

    public function blog_author() {
        return $this->hasOne('App\Models\Admin', 'id', 'added_by');
    }

    public function get_category(){
        return $this->hasMany('App\Models\PostCategory', 'post_id', 'id')->with('PostCategories');
    }

    public function get_tag(){
        return $this->hasMany('App\Models\PostTags', 'post_id', 'id')->with('postTags');
    }

    public function similarPost($postId, $categories) {
        return Posts::with('blog_author')->with('get_category')->select('title', 'description', 'viewed_count', 'added_by', 'featured_image', 'slug', 'status', 'created_at')->where([['id','!=',$postId],['status', '=', 'published']])->orderBy('created_at', 'desc')->limit(6)->get();
    }

    public function featuredPost($postId = null) {

        return Posts::with('blog_author')->with('get_category')->select('title','description', 'viewed_count', 'added_by', 'featured_image', 'slug', 'status', 'created_at')->where([['id','!=',$postId],['status', '=', 'published']])->orderBy('created_at', 'desc')->take(4)->get();
    }

    /**
     * fetch post in recent desc order
     */
    public function recentPost($postId = null, $limit = null){

        if($limit != ''){

            return Posts::with('blog_author')->with('get_category')->select('title','description', 'viewed_count', 'added_by','featured_image', 'slug', 'status', 'created_at')->where([['id','!=',$postId],['status', '=', 'published']])->orderBy('created_at', 'desc')->paginate($limit);
        } else {

            return Posts::with('blog_author')->with('get_category')->select('title','description', 'viewed_count', 'added_by','featured_image', 'slug', 'status', 'created_at')->where([['id','!=',$postId],['status', '=', 'published']])->orderBy('created_at', 'desc')->take(12)->get();
        }

    }

    /**
     * fetch post in popular
     */
    public function popularPost($postId = null){
        return Posts::with('blog_author')->with('get_category')->where([['id','!=',$postId],['status', '=', 'published']])->orderBy('viewed_count', 'desc')->take(10)->get();
    }

    /**
     * fetch post in archive
     */
    public function ArchivePosts(){
        return Posts::select(DB::raw('YEAR(created_at) year'), DB::raw('MONTHNAME(created_at) month_name'),DB::raw('MONTH(created_at) month'), DB::raw('count(*) as count'))->where('status', '=', 'published')->groupby('year', 'month_name', 'month')->get();
    }
}