<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|


Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
*/

// Authentication Routes
Route::post('login', 'AuthController@login')->name('login');
Route::post('register', 'AuthController@register');
Route::post('forgot-password', 'AuthController@forgot_password');

Route::group(['middleware' => 'auth:sanctum'], function () {
    // User Routes
    Route::get('profile', 'AuthController@profile');
    Route::put('change-password', 'AuthController@change_password');

    // Post Routes
    Route::get('/post/list', 'PostController@index');
    Route::get('/post/{id}', 'PostController@view');
    Route::post('/post/create', 'PostController@store');
    Route::put('/post/update/{id}', 'PostController@update');
    Route::delete('/post/destroy/{id}', 'PostController@destroy');
    Route::post('/post/upload', 'PostController@upload');

    // Category Routes
    Route::get('/category/list', 'CategoryController@index');
    Route::get('/category/{id}', 'CategoryController@view');
    Route::post('/category/create', 'CategoryController@store');
    Route::put('/category/update/{id}', 'CategoryController@update');
    Route::delete('/category/destroy/{id}', 'CategoryController@destroy');

    // Tag Routes
    Route::get('/tag/list', 'TagController@index');
    Route::get('/tag/{id}', 'TagController@view');
    Route::post('/tag/create', 'TagController@store');
    Route::put('/tag/update/{id}', 'TagController@update');
    Route::delete('/tag/destroy/{id}', 'TagController@destroy');

    // Course Type Routes
    Route::get('/course-type/list', 'CourseTypeController@index');
    Route::get('/course-type/{id}', 'CourseTypeController@view');
    Route::post('/course-type/create', 'CourseTypeController@store');
    Route::put('/course-type/update/{id}', 'CourseTypeController@update');
    Route::delete('/course-type/destroy/{id}', 'CourseTypeController@destroy');

    // Department Routes
    Route::get('/department/list', 'DepartmentController@index');
    Route::get('/department/{id}', 'DepartmentController@view');
    Route::post('/department/create', 'DepartmentController@store');
    Route::put('/department/update/{id}', 'DepartmentController@update');
    Route::delete('/department/destroy/{id}', 'DepartmentController@destroy');

    // Tag Routes
    Route::get('/tag/list', 'TagController@index');
    Route::get('/tag/{id}', 'TagController@view');
    Route::post('/tag/create', 'TagController@store');
    Route::put('/tag/update/{id}', 'TagController@update');
    Route::delete('/tag/destroy/{id}', 'TagController@destroy');

    Route::get('/contact-us/list', 'ContactUsController@index');

});

// Post Routes
Route::get('get-post/{slug}', 'PostController@detail');
Route::get('get-category-detail/{category_slug}', 'PostController@categoryDetails');
Route::get('get-category-posts/{category_slug}', 'PostController@categoryPosts');
Route::get('get-tag-posts/{tag_slug}', 'PostController@tagPosts');
Route::get('get-author-posts/{author_slug}', 'PostController@authorPosts');
Route::post('get-banners', 'BannerController@index');
Route::post('get-categories', 'CategoryController@index');
Route::get('get-recent', 'PostController@recent');
Route::get('get-popular', 'PostController@popular');
Route::get('get-posts', 'PostController@posts');
Route::get('get-search-posts', 'PostController@search');