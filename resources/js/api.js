// api.js
import axios from 'axios';

// Set up default headers for Axios
axios.defaults.headers.common['Access-Control-Allow-Origin'] = '*';
axios.defaults.headers.common['Access-Control-Allow-Methods'] = 'GET, POST, PUT, DELETE, OPTIONS';
axios.defaults.headers.common['Access-Control-Allow-Headers'] = 'X-Requested-With, Content-Type, Authorization';

const API_URL = 'http://laravel-vue/api/v1';

export default {
  // Authentication
  login(email, password, rememberMe) {
    return axios.post(`${API_URL}/login`, {
      email: email,
      password: password,
      remember_me: rememberMe  // Include remember_me parameter here
    });
  },
  changePassword(oldPassword, newPassword, confirmPassword) {
    const token = localStorage.getItem('token');
    return axios.put(`${API_URL}/change-password`, {
      old_password: oldPassword,
      new_password: newPassword,
      confirm_password: confirmPassword
    }, {
      headers: {
        Authorization: `Bearer ${token}`
      }
    });
  },
  // Post
  getPosts() {
    const token = localStorage.getItem('token');
    return axios.get(`${API_URL}/post/list`, {
      headers: {
        Authorization: `Bearer ${token}`
      }
    });
  },
  getPost(id) {
    const token = localStorage.getItem('token');
    return axios.get(`${API_URL}/post/${id}`, {
      headers: {
        Authorization: `Bearer ${token}`
      }
    });
  },
  createPost(data) {
    const token = localStorage.getItem('token');
    return axios.post(`${API_URL}/post/create`, data, {
      headers: {
        Authorization: `Bearer ${token}`
      }
    });
  },
  updatePost(id, data) {
    const token = localStorage.getItem('token');
    return axios.put(`${API_URL}/post/update/${id}`, data, {
      headers: {
        Authorization: `Bearer ${token}`
      }
    });
  },
  deletePost(id) {
    return axios.delete(`${API_URL}/post/${id}`, {
      headers: {
        Authorization: `Bearer ${token}`
      }
    });
  },

  uploadPostImage(data) {
    const token = localStorage.getItem('token');
    return axios.post(`${API_URL}/post/upload`, data, {
      headers: {
        'Authorization': `Bearer ${token}`
      }
    });
  },
  


  // Category
  getCategoryList(params) { 
    const token = localStorage.getItem('token');
    return axios.get(`${API_URL}/category/list`, {
      params,
      headers: {
        Authorization: `Bearer ${token}`
      }
    });
  },
  getCategory(id) {
    const token = localStorage.getItem('token');
    return axios.get(`${API_URL}/category/${id}`, {
      headers: {
        Authorization: `Bearer ${token}`
      }
    });
  },
  createCategory(data) {
    const token = localStorage.getItem('token');
    return axios.post(`${API_URL}/category/create`, data, {
      headers: {
        Authorization: `Bearer ${token}`
      }
    });
  },
  updateCategory(id, data) {
    const token = localStorage.getItem('token');
    return axios.put(`${API_URL}/category/${id}`, data, {
      headers: {
        Authorization: `Bearer ${token}`
      }
    });
  },
  deleteCategory(id) {
    const token = localStorage.getItem('token');
    return axios.delete(`${API_URL}/category/${id}`, {
      headers: {
        Authorization: `Bearer ${token}`
      }
    });
  },

  // Tags
  getTagList(params) { 
    const token = localStorage.getItem('token');
    return axios.get(`${API_URL}/tag/list`, {
      params,
      headers: {
        Authorization: `Bearer ${token}`
      }
    });
  },

  // CourseTypes
  getCourseTypeList(params) { 
    const token = localStorage.getItem('token');
    return axios.get(`${API_URL}/course-type/list`, {
      params,
      headers: {
        Authorization: `Bearer ${token}`
      }
    });
  },
  getCourseType(id) {
    const token = localStorage.getItem('token');
    return axios.get(`${API_URL}/course-type/${id}`, {
      headers: {
        Authorization: `Bearer ${token}`
      }
    });
  },
  createCourseType(data) {
    const token = localStorage.getItem('token');
    return axios.post(`${API_URL}/course-type/create`, data, {
      headers: {
        Authorization: `Bearer ${token}`
      }
    });
  },
  updateCourseType(id, data) {
    const token = localStorage.getItem('token');
    return axios.put(`${API_URL}/course-type/update/${id}`, data, {
      headers: {
        Authorization: `Bearer ${token}`
      }
    });
  },
  deleteCourseType(id) {
    const token = localStorage.getItem('token');
    return axios.delete(`${API_URL}/course-type/${id}`, {
      headers: {
        Authorization: `Bearer ${token}`
      }
    });
  },

  // Department
  getDepartmentList(params) { 
    const token = localStorage.getItem('token');
    return axios.get(`${API_URL}/department/list`, {
      params,
      headers: {
        Authorization: `Bearer ${token}`
      }
    });
  },
  getDepartment(id) {
    const token = localStorage.getItem('token');
    return axios.get(`${API_URL}/department/${id}`, {
      headers: {
        Authorization: `Bearer ${token}`
      }
    });
  },
  createDepartment(data) {
    const token = localStorage.getItem('token');
    return axios.post(`${API_URL}/department/create`, data, {
      headers: {
        Authorization: `Bearer ${token}`
      }
    });
  },
  updateDepartment(id, data) {
    const token = localStorage.getItem('token');
    return axios.put(`${API_URL}/department/update/${id}`, data, {
      headers: {
        Authorization: `Bearer ${token}`
      }
    });
  },
  deleteDepartment(id) {
    const token = localStorage.getItem('token');
    return axios.delete(`${API_URL}/department/${id}`, {
      headers: {
        Authorization: `Bearer ${token}`
      }
    });
  },
  // ContactUs
  getContactUsList(params = {}) {
    const token = localStorage.getItem('token');
    return axios.get(`${API_URL}/contact-us/list`, {
      params,
      headers: {
        Authorization: `Bearer ${token}`
      }
    });
  },
  deleteContactUs(id) {
    const token = localStorage.getItem('token');
    return axios.delete(`${API_URL}/contact-us/${id}`, {
      headers: {
        Authorization: `Bearer ${token}`
      }
    });
  },
};
