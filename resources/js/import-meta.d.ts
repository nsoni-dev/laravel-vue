// Create a declaration file (e.g., import-meta.d.ts)
// This file should be in your TypeScript project

// Define a custom interface to extend import.meta
interface CustomImportMeta {
    VITE_APP_NAME: string;
    VITE_APP_URL: string;
    // Add any other properties you need to extend import.meta with
  }
  
  // Augment the existing ImportMeta type
  declare global {
    interface ImportMeta extends CustomImportMeta {}
  }
  