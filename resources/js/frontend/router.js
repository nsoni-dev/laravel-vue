// router.js

import { createRouter, createWebHistory } from 'vue-router';
import FrontendLayout from './FrontendLayout.vue';

import Dashboard from './components/Dashboard.vue';
import TaskList from './components/Task/TaskList.vue';
import TaskForm from './components/Task/TaskForm.vue';
import TaskView from './components/Task/TaskView.vue';

import ProjectList from './components/Project/ProjectList.vue';
import ProjectForm from './components/Project/ProjectForm.vue';
import ProjectView from './components/Project/ProjectView.vue';

import CompanyList from './components/Company/CompanyList.vue';
import CompanyForm from './components/Company/CompanyForm.vue';
import CompanyView from './components/Company/CompanyView.vue';
const routes = [
  {
    path: '/',
    component: FrontendLayout,
    children: [
      {
        path: 'dashboard',
        component: Dashboard,
      },
	  {
	    path: '/tasks',
	    component: TaskList,
	    name: 'task-list',
	  },
	  {
	    path: '/task/create',
	    component: TaskForm,
	    name: 'task-create',
	  },
	  {
	    path: '/task/edit/:taskId',
	    component: TaskForm,
	    name: 'task-edit',
	  },
	  {
	    path: '/task/view/:taskId',
	    component: TaskView,
	    name: 'task-view',
	  },

	  {
	    path: '/projects',
	    component: ProjectList,
	    name: 'project-list',
	  },
	  {
	    path: '/project/create',
	    component: ProjectForm,
	    name: 'project-create',
	  },
	  {
	    path: '/project/edit/:projectId',
	    component: ProjectForm,
	    name: 'project-edit',
	  },
	  {
	    path: '/project/view/:projectId',
	    component: ProjectView,
	    name: 'project-view',
	  },

	  {
	    path: '/companies',
	    component: CompanyList,
	    name: 'company-list',
	  },
	  {
	    path: '/company/create',
	    component: CompanyForm,
	    name: 'company-create',
	  },
	  {
	    path: '/company/edit/:companyId',
	    component: CompanyForm,
	    name: 'company-edit',
	  },
	  {
	    path: '/company/view/:companyId',
	    component: CompanyView,
	    name: 'company-view',
	  },
    ],
  },
];

const router = createRouter({
	history: createWebHistory(),
	routes,
  });
  
export default router;