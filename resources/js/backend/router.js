import { createRouter, createWebHistory } from 'vue-router';
import AdminLayout from './AdminLayout.vue';

import Login from './components/Login.vue';
import Register from './components/Register.vue';
import ResetPassword from './components/ResetPassword.vue';
import ForgotPassword from './components/ForgotPassword.vue';
import Dashboard from './components/Dashboard.vue';
import ChangePassword from './components/ChangePassword.vue';


import PostList from './components/Post/List.vue';
import PostCreate from './components/Post/Create.vue';
import PostEdit from './components/Post/Edit.vue';
import PostView from './components/Post/View.vue';

import CategoryList from './components/Category/List.vue';
import CategoryCreate from './components/Category/Create.vue';
import CategoryEdit from './components/Category/Edit.vue';
import CategoryView from './components/Category/View.vue';

import TagList from './components/Tag/List.vue';
import TagCreate from './components/Tag/Create.vue';
import TagEdit from './components/Tag/Edit.vue';
import TagView from './components/Tag/View.vue';

import CourseTypeList from './components/CourseType/List.vue';
import CourseTypeCreate from './components/CourseType/Create.vue';
import CourseTypeEdit from './components/CourseType/Edit.vue';
import CourseTypeView from './components/CourseType/View.vue';

import CourseList from './components/Course/List.vue';
import CourseCreate from './components/Course/Create.vue';
import CourseEdit from './components/Course/Edit.vue';
import CourseView from './components/Course/View.vue';

import DepartmentList from './components/Department/List.vue';
import DepartmentCreate from './components/Department/Create.vue';
import DepartmentEdit from './components/Department/Edit.vue';
import DepartmentView from './components/Department/View.vue';

import ContactUsList from './components/ContactUs/List.vue';
// Function to check if user is authenticated
function isAuthenticated() {
  // Check if the user is authenticated, e.g., by checking if the token exists in local storage
  const token = localStorage.getItem('token');
  return !!token;
}

const router = createRouter({
  history: createWebHistory(),
  routes: [
    // Admin Routes
    {
      path: '/admin',
      component: AdminLayout,
      children: [
        {
          path: 'dashboard',
          component: Dashboard,
          name: 'admin-dashboard',
          meta: { requiresAuth: true },
        },
        {
          path: 'change-password',
          component: ChangePassword,
          name: 'admin-change-password',
          meta: { requiresAuth: true },
        },
        {
          path: 'post/list',
          component: PostList,
          name: 'admin-post-list',
          meta: { requiresAuth: true },
        },
        {
          path: 'post/create',
          component: PostCreate,
          name: 'admin-post-create',
          meta: { requiresAuth: true },
        },
        {
          path: 'post/edit/:postId',
          component: PostEdit,
          name: 'admin-post-edit',
          meta: { requiresAuth: true },
        },
        {
          path: 'post/view/:postId',
          component: PostView,
          name: 'admin-post-view',
          meta: { requiresAuth: true },
        },
        {
          path: 'category/list',
          component: CategoryList,
          name: 'admin-category-list',
          meta: { requiresAuth: true },
        },
        {
          path: 'category/create',
          component: CategoryCreate,
          name: 'admin-category-create',
          meta: { requiresAuth: true },
        },
        {
          path: 'category/edit/:categoryId',
          component: CategoryEdit,
          name: 'admin-category-edit',
          meta: { requiresAuth: true },
        },
        {
          path: 'category/view/:categoryId',
          component: CategoryView,
          name: 'admin-category-view',
          meta: { requiresAuth: true },
        },
        {
          path: 'tag/list',
          component: TagList,
          name: 'admin-tag-list',
          meta: { requiresAuth: true },
        },
        {
          path: 'tag/create',
          component: TagCreate,
          name: 'admin-tag-create',
          meta: { requiresAuth: true },
        },
        {
          path: 'tag/edit/:tagId',
          component: TagEdit,
          name: 'admin-tag-edit',
          meta: { requiresAuth: true },
        },
        {
          path: 'tag/view/:tagId',
          component: TagView,
          name: 'admin-tag-view',
          meta: { requiresAuth: true },
        },
        {
          path: 'course-type/list',
          component: CourseTypeList,
          name: 'admin-course-type-list',
          meta: { requiresAuth: true },
        },
        {
          path: 'course-type/create',
          component: CourseTypeCreate,
          name: 'admin-course-type-create',
          meta: { requiresAuth: true },
        },
        {
          path: 'course-type/edit/:courseTypeId',
          component: CourseTypeEdit,
          name: 'admin-course-type-edit',
          meta: { requiresAuth: true },
        },
        {
          path: 'course-type/view/:courseTypeId',
          component: CourseTypeView,
          name: 'admin-course-type-view',
          meta: { requiresAuth: true },
        },
        {
          path: 'course/list',
          component: CourseList,
          name: 'admin-course-list',
          meta: { requiresAuth: true },
        },
        {
          path: 'course/create',
          component: CourseCreate,
          name: 'admin-course-create',
          meta: { requiresAuth: true },
        },
        {
          path: 'course/edit/:courseId',
          component: CourseEdit,
          name: 'admin-course-edit',
          meta: { requiresAuth: true },
        },
        {
          path: 'course/view/:courseId',
          component: CourseView,
          name: 'admin-course-view',
          meta: { requiresAuth: true },
        },
        {
          path: 'department/list',
          component: DepartmentList,
          name: 'admin-department-list',
          meta: { requiresAuth: true },
        },
        {
          path: 'department/create',
          component: DepartmentCreate,
          name: 'admin-department-create',
          meta: { requiresAuth: true },
        },
        {
          path: 'department/edit/:departmentId',
          component: DepartmentEdit,
          name: 'admin-department-edit',
          meta: { requiresAuth: true },
        },
        {
          path: 'department/view/:departmentId',
          component: DepartmentView,
          name: 'admin-department-view',
          meta: { requiresAuth: true },
        },


        {
          path: 'contact-us/list',
          component: ContactUsList,
          name: 'admin-contactus-list',
          meta: { requiresAuth: true },
        },
        
      ],
    },
    {
      path: '/admin/login',
      component: Login,
      name: 'admin-login',
      beforeEnter: (to, from, next) => {
        if (isAuthenticated()) {
          next('/admin/dashboard');
        } else {
          next();
        }
      },
    },
    {
      path: '/admin/register',
      component: Register,
      name: 'admin-register',
      beforeEnter: (to, from, next) => {
        if (isAuthenticated()) {
          next('/admin/dashboard');
        } else {
          next();
        }
      },
    },
    {
      path: '/admin/reset-password',
      component: ResetPassword,
      name: 'admin-reset-password',
      beforeEnter: (to, from, next) => {
        if (isAuthenticated()) {
          next('/admin/dashboard');
        } else {
          next();
        }
      },
    },
    {
      path: '/admin/forgot-password',
      component: ForgotPassword,
      name: 'admin-forgot-password',
      beforeEnter: (to, from, next) => {
        if (isAuthenticated()) {
          next('/admin/dashboard');
        } else {
          next();
        }
      },
    },
  ],
});

// Add a global navigation guard
router.beforeEach((to, from, next) => {
  // Check if the route requires authentication
  if (to.matched.some(record => record.meta.requiresAuth)) {
    // Check if the user is authenticated
    if (isAuthenticated()) {
      next(); // Proceed to the route if authenticated
    } else {
      next('/admin/login'); // Redirect to login if not authenticated
    }
  } else {
    next(); // Proceed to the route if authentication is not required
  }
});

export default router;
