// Import Vue and the createApp function
import { createApp } from 'vue';
// Import the frontend components and router
import FrontendApp from './frontend/App.vue'; // Frontend Vue instance
import FrontendRouter from './frontend/router'; // Frontend router configuration
// Import the backend components and router
import BackendApp from './backend/App.vue'; // Backend Vue instance
import BackendRouter from './backend/router'; // Backend router configuration


// Create the frontend Vue application instance
const frontendApp = createApp(FrontendApp);
// Mount the frontend app with the frontend router
frontendApp.use(FrontendRouter);
// Mount the frontend app to the DOM element with id 'frontend-app'
frontendApp.mount('#frontend-app');

// Create the backend Vue application instance
const backendApp = createApp(BackendApp);
// Mount the backend app with the backend router
backendApp.use(BackendRouter);

// Mount the backend app to the DOM element with id 'backend-app'
backendApp.mount('#backend-app');
