<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
  <head>
      <meta charset="utf-8">
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta name="csrf-token" content="{{ csrf_token() }}" />
      <title>{{ config('app.name', 'Social App') }}</title>
      <link rel="shortcut icon" href="{{URL:: asset('images/favicon.ico')}}">
      <!-- Bootstrap Css -->
      <link href="{{URL::asset('admin/css/bootstrap.min.css')}}" id="bootstrap-style" rel="stylesheet" type="text/css" />
      <!-- Icons Css -->
      <link href="{{URL::asset('admin/css/icons.min.css')}}" rel="stylesheet" type="text/css" />
      <!-- App Css-->
      <link href="{{URL::asset('admin/css/app.min.css')}}" id="app-style" rel="stylesheet" type="text/css" />
        <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
      @vite(['resources/js/app.js'])
  </head>
  <body data-sidebar="dark" data-layout-mode="light">
      <div id="backend-app">

      </div>
  </body>
</html>