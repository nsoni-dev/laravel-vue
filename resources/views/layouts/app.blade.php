<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>{{ config('app.name', 'Bloomex Inc.') }}</title>
    <link rel="shortcut icon" href="{{URL:: asset('filemanager/images/fevicon.png')}}">
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
    <!-- Global stylesheets -->
    <link href="{{URL::asset('css/icons/icomoon/styles.css')}}" rel="stylesheet" type="text/css">
    <link href="{{URL::asset('css/bootstrap.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{URL::asset('css/bootstrap_limitless.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{URL::asset('css/layout.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{URL::asset('css/components.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{URL::asset('css/colors.min.css')}}" rel="stylesheet" type="text/css">
    @vite(['resources/js/app.js', 'resources/css/app.css'])
    <script type="text/javascript" src="{{URL::asset('js/main/jquery.min.js')}}"></script>
</head>
<body>
    <div id="frontend-app">

    </div>
    <script type="text/javascript" src="{{URL::asset('js/main/bootstrap.bundle.min.js')}}"></script>
    <script type="text/javascript" src="{{URL::asset('js/plugins/loaders/blockui.min.js')}}"></script>

    <script type="text/javascript" src="{{URL::asset('js/main/bootstrap-confirmation.min.js') }}"></script>
    <script type="text/javascript" src="{{URL::asset('js/plugins/forms/selects/bootstrap_multiselect.js')}}"></script>
    <script type="text/javascript" src="{{URL::asset('js/script.js')}}"></script>
</body>
</html>