git clone origin https://gitlab.com/nsoni-dev/bloomex-test.git
cd bloomex-test
sudo cp .env.example .env
composer update
sudo chmod -R 777 storage
sudo chmod -R 777 bootstrap
npm update

create a database name "bloomex"
update database details into .env

run php artisan migrate
php artisan db:seed

after update .env run composer dump-autoload

sudo nano +4 resources/js/api.js
update bath path at line number 4