const mix = require('laravel-mix');

mix.js('resources/js/app.js', 'public/js')
   .postCss('resources/css/app.css', 'public/css')
   .vite(['resources/js/app.js', 'resources/css/app.css']);

