$(function() {

  $('[data-toggle=confirmation]').confirmation({
    rootSelector: '[data-toggle=confirmation]',
    // other options
  });

  /*Add Course Page*/
  $('.add-more-view').click(function(){ 
     var len = $(this).parent().find('input').length;
     $(this).parent().append('<div class="row" style="margin:10px;"><div class="col-md-10"><input type="text" class="form-control"></div><div class="col-md-2"><button class="btn btn-danger">Remove</buttton></div></div>');
     alert(len);
  });

  $('.all-checkbox').change(function () {
     if ($(this).is(':checked')) {
          $('.checkbox').prop('checked', true);
      } else {
          $('.checkbox').prop('checked',false);
      }
  });

  $('#course-category-action').change(function(){
    var action = $(this).val();
    var action_url = $('#action_url').val();
    var ids = $("input[name=chkboxName]:checked").map(function() {
      return this.value;
    }).get().join(",");
    if(action != 'not-check' && ids){
      $.ajax({
          url: action_url,
          type: "get",
          data: {'action':action,'ids':ids} ,
          success: function (response) {                 
             var Result = JSON.parse(response);
             switch(Result.status){
                 case 1:
                     var main_url = $('#main_url').val();
                     window.location.href = main_url;
                     break;
                 case 0:
                 alert(Result.message);
                     break;
                  default:
                 
             }
          }
      });
    }
  });

  $(".number_only").on('keydown',function(event) {
      // Allow: dot, backspace, delete, tab, decimal point,escape, and enter
      if ( event.keyCode == 190 || event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 110 || event.keyCode == 27 || event.keyCode == 13 ||
      // Allow: Ctrl+A
      (event.keyCode == 65 && event.ctrlKey === true) ||
      // Allow: home, end, left, right
      (event.keyCode >= 35 && event.keyCode <= 39)) {
               // let it happen, don't do anything
              return;
      }
      else
      {
          // Ensure that it is a number and stop the keypress
          if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
              event.preventDefault();
          }  
      }
  });

  $('.check_email').on('change', function(){
    var email = $(this).val();
    if (!validateEmail(email)) {
        alert('Please Enter a valid email');
        $(this).val('');
      }
  });

$('.all-checkbox').change(function () {
   if ($(this).is(':checked')) {
        $('.checkbox').prop('checked', true);
    } else {
        $('.checkbox').prop('checked',false);
    }
});

$('#page-action').change(function(){
  var action = $(this).val();
  var action_url = $('#action_url').val();
  var ids = $("input[name=chkboxName]:checked").map(function() {
    return this.value;
  }).get().join(",");
  if(action != 'not-check' && ids){
    $.ajax({
        url: action_url,
        type: "get",
        data: {'action':action,'ids':ids} ,
        success: function (response) {                 
           var Result = JSON.parse(response);
           switch(Result.status){
               case 1:
                   var main_url = $('#main_url').val();
                   window.location.href = main_url;
                   break;
               case 0:
               alert(Result.message);
                   break;
                default:
               
           }
        }
    });
  }
});

  $('#page-records').change(function(){
    var per_page = $(this).val();
    var per_page_url = $('#per-page-url').val();
    if(per_page != 'not-check' && per_page_url){
      $.ajax({
        url: per_page_url,
        type: "get",
        data: {'per_page':per_page},
        success: function (response) {
          var Result = JSON.parse(response);
          switch(Result.status){
            case 1:
              var main_url = $('#main_url').val();
              window.location.href = main_url;
            break;
            case 0:
              alert(Result.message);
            break;
            default:
          }
        }
      });
    } 
  });

});

function validateEmail(field) {
  var regex = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,5}$/;
  return (regex.test(field)) ? true : false;
}