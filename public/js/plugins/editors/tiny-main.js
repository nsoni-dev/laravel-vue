$(document).ready(function()
{
  var setImageData = function(contentDocument){
    var $contentDocument = $( contentDocument );
    var $imgs = $contentDocument.find('img');
    $imgs.each(function(i,obj){
      var $img = $(obj);
      if( !($img.is('[src*="blob:"],img[src*="data:"]')) ){
        $img.attr('data-original-src', $img.attr('src'));
      }
    });
  };

  tinymce.PluginManager.add('twitter', function(editor, url) {
    var icon_url='img/social/instagram.png';
    editor.on('init', function (args) {
      editor_id = args.target.id;
    });
    editor.addButton('twitter',{
      text:false,
      icon: true,
      image:icon_url,
      onclick: function () {
        editor.windowManager.open({
          title: 'Twitter Embed',
          body: [
            {
              type: 'textbox',
              size: 40,
              height: '100px',
              name: 'twitter',
              label: 'twitter'
            }
          ],
          onsubmit: function(e) {
            var tweetEmbedCode = e.data.twitter;
            $.ajax({
              url: "https://publish.twitter.com/oembed?url="+tweetEmbedCode,
              dataType: "jsonp",
              async: false,
              success: function(data){
                // $("#embedCode").val(data.html);
                // $("#preview").html(data.html)
                tinyMCE.activeEditor.insertContent(
                    '<div class="div_border" contenteditable="false">' +
                        '<img class="twitter-embed-image" src="'+icon_url+'" alt="image" />'
                        +data.html+
                    '</div>');

              },
              error: function (jqXHR, exception) {
                var msg = '';
                if (jqXHR.status === 0) {
                  msg = 'Not connect.\n Verify Network.';
                } else if (jqXHR.status == 404) {
                  msg = 'Requested page not found. [404]';
                } else if (jqXHR.status == 500) {
                  msg = 'Internal Server Error [500].';
                } else if (exception === 'parsererror') {
                  msg = 'Requested JSON parse failed.';
                } else if (exception === 'timeout') {
                  msg = 'Time out error.';
                } else if (exception === 'abort') {
                  msg = 'Ajax request aborted.';
                } else {
                  msg = 'Uncaught Error.\n' + jqXHR.responseText;
                }
                alert(msg);
              },
            });
            setTimeout(function() {
              iframe.contentWindow.twttr.widgets.load();
            }, 1000)
          }
        });
      }
    });
  });

  tinymce.PluginManager.add('instagram', function(editor, url) {
    // Add a button that opens a window
    var icon_url='img/social/instagram.png';
    editor.addButton('instagram', {
      text:false,
      icon:true,
      image:icon_url,
      onclick: function() {
        // Open window
        editor.windowManager.open({
          title: 'Instagram Embed',
          body: [
            {
              type: 'textbox',
              size: 40,
              height: '100px',
              name: 'instagram',
              label: 'content'
            }
          ],
          onsubmit: function(e) {
            // Insert content when the window form is submitted
            console.log(e.data.instagram);
            var embedCode = e.data.instagram;
            var script = embedCode.match(/<script.*<\/script>/)[0];
            var scriptSrc = script.match(/".*\.js/)[0].split("\"")[1];

            var sc = document.createElement("script");
            sc.setAttribute("src", scriptSrc);
            sc.setAttribute("type", "text/javascript");

            var iframe = document.getElementById(editor_id + "_ifr");
            var iframeHead = iframe.contentWindow.document.getElementsByTagName('head')[0];

            embedCode1 = embedCode.replace('//platform.instagram.com/en_US/embeds.js','https://platform.instagram.com/en_US/embeds.js');

            tinyMCE.activeEditor.insertContent(embedCode1);
            iframeHead.appendChild(sc);
            setTimeout(function()
            {
              iframe.contentWindow.instgrm.Embeds.process();
            }, 1000)
          }
        });
      }
    });
  });

  tinymce.PluginManager.add('facebook', function(editor, url) {
    // Add a button that opens a window
    var icon_url='img/social/instagram.png';
    editor.addButton('facebook', {
      text:false,
      icon:true,
      image:icon_url,
      onclick: function() {
        // Open window
        editor.windowManager.open({
          title: 'facebook Embed',
          body: [
            {   
              type: 'textbox',
              size: 40,
              height: '100px',
              name: 'instagram1',
              label: 'instagram1'
            }
          ],
          onsubmit: function(e)
          {
            //console.log(e.data.youtube);
            var embedCode = e.data.instagram1;
            var data='<div contenteditable="false"><iframe allowtransparency="true" frameborder="0" height="690" scrolling="no" src="'+embedCode+'/embed/captioned" width="500" style="border:none;overflow:hidden"></iframe></div>';
            tinymce.activeEditor.insertContent(data);
            setTimeout(function() {
                //iframe.contentWindow.twttr.widgets.load();
            }, 1000)
            // editor.insertContent('Title: ' + e.data.title);
          }
        });
      }
    });
  });

  tinymce.init({
    selector: 'textarea#ckeditor',
    height: 500,
    setup: function (editor) {
      editor.on('init change', function () {
        editor.save();
      });
    },

    plugins:'preview code twitter instagram facebook',
    toolbar1:'preview code twitter instagram facebook',
    content_css: [
      'css/main.css?' + new Date().getTime(),
      '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
      '//www.tinymce.com/css/codepen.min.css'
    ],
    //extended_valid_elements : 'img[class|src|border=0|alt|title|hspace|vspace|width|height|align|onmouseover|onmouseout|name]',

    extended_valid_elements: "+iframe[width|height|name|align|class|frameborder|allowfullscreen|allow|src|*]," +
    "script[language|type|async|src|charset]" +
    "img[*]" +
    "embed[width|height|name|flashvars|src|bgcolor|align|play|loop|quality|allowscriptaccess|type|pluginspage]" +
    "blockquote[dir|style|cite|class|id|lang|onclick|ondblclick"
    +"|onkeydown|onkeypress|onkeyup|onmousedown|onmousemove|onmouseout"
    +"|onmouseover|onmouseup|title]",

    valid_elements : '+*[*]',

    mediaembed_service_url: "SERVICE_URL",
    mediaembed_max_width: 450,
    setup: function(editor) {
      //register callbacks to capture the image's original-src at appropriate times
      editor.on('BeforeSetContent', function(e) {
        setImageData( e.target.contentDocument );
      });
      editor.on('change', function(e) {
        setImageData( e.target.contentDocument );
      });
      editor.on('init', function(e) { 
        setImageData( e.target.contentDocument );
        editor_id = e.target.id;
      });
      editor.on('GetContent', function(e) {
        //inline-image upload sometimes leaves empty <img> tags in the dom if event is
        //cancelled. we don't want those.
        var html = e.content;
        if(html){
          var $html = $('<div>'+html+'</div>'); //wrap a div so jquery works right
          var $removeRegions = $html.find("img[src=''], img:not([src])");
          if($removeRegions.length > 0){
            $removeRegions.remove();
          }

          //cleanup if this is a 'save' event
          //for some reason, the SaveContent callback doesn't always save the changes...
          //haven't figured that one out. this is the workaround.
          if(e.save){
            //clean up 'data-original-src' attributes for our image uploader
            $html.find("img").removeAttr('data-original-src');
          }
          else{
            html = $html.html();
          }

          e.content = html;
        }
      });
      
      editor.on('SaveContent', function(e) {
        //inline-image upload sometimes leaves empty <img> tags in the dom if event is
        //cancelled. we don't want those.
        var html = e.content;
        if(html){
          var $html = $('<div>'+html+'</div>'); //wrap a div so jquery works right
          var $removeRegions = $html.find("img[src=''], img:not([src])");
          if($removeRegions.length > 0){
            $removeRegions.remove();
            html = $html.html();
          }

          //clean up 'data-original-src' attributes for our image uploader
          $html.find("img").removeAttr('data-original-src');

          e.content = html;
        }
      });
    },
    //handle upload of new/existing images
    images_upload_handler : function (blobInfo, success, failure) {
      //get current tinymce editor
      var editor = window.tinymce.activeEditor;
      //get image we're editing - the src is "blob:" or "data:"
      var $img = $(editor.contentDocument).find('img[src*="blob:"],img[src*="data:"]');

      //see if we already have the 'data-original-src' to use for our existing filename
      var imgOrigSrc = $img.attr('data-original-src');

      //if we don't have original source, assume it's a new file
      var defaultFilename = (imgOrigSrc ? false : true);

      //get only the image name and exclude the rest of the url path
      var imageFilename = (imgOrigSrc ? imgOrigSrc.split('/').pop() : '');

      //overwrite by default if we know the filename already
      var overwrite = (imageFilename ? true : false);
      //ask user for filename
      var filename = imageFilename || blobInfo.filename();
      var filenameSplit = filename.split('.');
      var extension = filenameSplit.pop();
      var filenameNoExtension = filenameSplit.join('.');
      var html = (overwrite
              ? '<h1>Overwrite or rename this image:</h1>' 
              : '<h1>Give this image a relevant filename:</h1>')
          +'<input name="filename" class="filename" type="text" size="35" '
              + ' value="' + (defaultFilename ? '' : filenameNoExtension) + '" />.'+ extension
          +(overwrite ? 
              '<br /><label><input name="overwrite" value="1" type="checkbox" class="overwrite"'
              + ' checked="checked" />Overwrite</label>' : '');
      //...
      // use 'html' (above) to present some sort dialog for the user to input a filename,
      // then submit ajax to save the image (below)
      //... 
      //attach image to form for POST
      var formData = new FormData();
      formData.append('file', blobInfo.blob(), filename);
      formData.append('overwrite', (overwrite ? '1' : '0') ); //our backend script uses this
      //do ajax to save image
      $.ajaxSetup({
        headers: { 'X-CSRF-TOKEN' : '{{csrf_token()}}' }, 
      });
      $.ajax({
        type: 'POST',
        url: "{{route('admin.post.upload')}}",
        data: formData,
        dataType: 'json',
        contentType: false,
        processData: false,    
        cache: false,
        success: function(result, textStatusStr, jqXHR){
          //we actually check here if the filename is in use, and re-show the dialog
          //to the user asking for filename if it is.
          //this gives the uesr an opportunity to override or choose a new filename.
          //leaving this code out for simplicity though.
          
          //get image we're editing and set our "original-src" attribute.
          //the current src of this image is "blob:" or "data:"
          var $img = $(editor.contentDocument).find('img[src*="blob:"],img[src*="data:"]');
          $img.attr('data-original-src', result.location);
          //trigger the tinymce callback
          //finalImageLocation as our custom backend script returns
          success(result.location);
        },
        error: function(jqXHR, textStatusStr, errorThrownStr){
          //ajax request failed entirely
          //undo tinymce changes
          editor.undoManager.undo();
          //trigger the tinymce callback
          failure("HTTP Error: " + textStatusStr);
        }
      });
    }
  });
});