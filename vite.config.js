// vite.config.js
import { defineConfig } from 'vite';
import laravel from 'laravel-vite-plugin';
import vue from '@vitejs/plugin-vue';

export default defineConfig({
    plugins: [
        laravel({
            input: [
                'resources/sass/app.scss',
                'resources/js/app.js',
            ],
            refresh: true,
        }),
        vue({
            template: {
                transformAssetUrls: {
                    base: null,
                    includeAbsolute: false,
                },
            },
        }),
    ],
    resolve: {
        alias: {
            '@': '/resources/js',
            'vue': 'vue/dist/vue.runtime.esm-bundler.js',
            'jquery': 'jquery',
        },
    },
    build: {
        sourcemap: true,
        chunkSizeWarningLimit: 2000,
        rollupOptions: {
            external: ['jquery', '@ckeditor/ckeditor5-vue3'],
            output: {
                manualChunks(id) {
                    if (id.includes('node_modules')) {
                        return 'vendor';
                    }
                }
            }
        },
    },
    server: {
        mimeTypes: {
            'application/javascript': ['js']
        },
        // Proxy API requests
        proxy: {
            '/api': {
                target: 'http://laravel-vue', // Example target URL
                changeOrigin: true,
                pathRewrite: { '^/api': '' }
            }
        },
        // Set custom headers
        headers: {
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, PATCH, OPTIONS',
            'Access-Control-Allow-Headers': 'X-Requested-With, content-type, Authorization'
        }
    },
    optimizeDeps: {
        exclude: ['@popperjs/core'],
    },
});
